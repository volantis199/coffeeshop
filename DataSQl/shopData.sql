﻿CREATE DATABASE CoffeeShop
go --cứ thấy go là thực hiện lệnh đằng trc

USE CoffeeShop
GO
-- Food Table
-- TableNumber Table
-- Food Category
-- Account - used to sign in
-- Bill && Bill Info: used to determine food type

CREATE TABLE tableSeat
(
	id INT IDENTITY PRIMARY KEY NOT NULL, -- tự tạo id kiểu int và tự tăng do identity
	name NVARCHAR(100) NOT NULL,
	status NVARCHAR(100) NOT NULL DEFAULT N'Trống', -- bàn trống hay có người , etc
)
GO

CREATE TABLE Account
(
	userName NVARCHAR(100) PRIMARY KEY,
	displayName NVARCHAR(100) NOT NULL DEFAULT N'user' ,
	password NVARCHAR(1000) NOT NULL DEFAULT 0,
	accountType INT NOT NULL DEFAULT 0
	--1: admin || 0 : staffs
)
GO

CREATE TABLE foodCategory
(
	id INT IDENTITY PRIMARY KEY NOT NULL,
	name NVARCHAR(30) NOT NULL DEFAULT N'chưa đặt tên'
)
GO

CREATE TABLE Food
(
	id INT IDENTITY PRIMARY KEY NOT NULL,
	name NVARCHAR(30) NOT NULL DEFAULT N'chưa đặt tên',
	idCategory INT NOT NULL,
	price FLOAT NOT NULL

	FOREIGN KEY (idCategory) REFERENCES dbo.foodCategory(id)

)
GO

CREATE TABLE Bill
(
	id INT IDENTITY PRIMARY KEY NOT NULL,
	-- cần biết bill cho bàn nào và thời gian xác định
	dateCheckIn DATE NOT NULL DEFAULT GETDATE(), -- hàm getdate() lấy ngày hiện tại
	dateCheckOut DATE NOT NULL,
	idTable INT  NOT NULL,
	status INT  NOT NULL DEFAULT 0, -- 1: đã thanh toán, 0: chưa thanh toán

	FOREIGN KEY (idTable) REFERENCES dbo.tableSeat(id)
)
GO

CREATE TABLE billInfo
(
	id INT IDENTITY PRIMARY KEY NOT NULL,
	idBill INT NOT NULL,
	idFood INT NOT NULL,
	quantity INT NOT NULL DEFAULT 0

	FOREIGN KEY (idBill) REFERENCES dbo.Bill(id),
	FOREIGN KEY (idFood) REFERENCES dbo.Food(id)
)
GO

INSERT INTO dbo.Account
(
    userName,
    displayName,
    password,
    accountType
)
VALUES
(   N'admin', -- userName - nvarchar(100)
    N'Ad', -- displayName - nvarchar(100)
    N'admin', -- password - nvarchar(1000)
    1    -- accountType - int
    )

	INSERT INTO dbo.Account
(
    userName,
    displayName,
    password,
    accountType
)
VALUES
(   N'staff', -- userName - nvarchar(100)
    N'staff', -- displayName - nvarchar(100)
    N'staff', -- password - nvarchar(1000)
    0    -- accountType - int
    )
GO

CREATE PROCEDURE UserLogin
@userName NVARCHAR(100),
@password NVARCHAR(100)
AS
BEGIN
	SELECT * FROM dbo.Account WHERE userName = @userName AND password = @password
END
GO

DECLARE @i INT = 1
WHILE @i <= 10
BEGIN
	INSERT INTO dbo.tableSeat
	(name)
	VALUES
	(N'Bàn ' + CAST(@i AS NVARCHAR(100)))
	SET @i = @i + 1
END
GO

CREATE PROCEDURE GetTableList
AS
BEGIN
	SELECT* FROM dbo.tableSeat
END
GO
--thêm category

ALTER TABLE dbo.Bill
ALTER COLUMN dateCheckOut SMALLDATETIME NULL
GO

--thêm trường Discount cho bill vằ đặt lại bằng 0
ALTER TABLE dbo.Bill
ADD discount INT
GO
ALTER TABLE dbo.Bill
ADD totalPrice int
go
UPDATE dbo.Bill SET discount = 0;
GO

--tạo stored procedure để thêm Bill
CREATE PROC InsertBill
@idTable Int
AS
BEGIN
	INSERT INTO dbo.Bill
	(
	    dateCheckIn,
	    dateCheckOut,
	    idTable,
	    status,
		discount
	)
	VALUES
	(   GETDATE(),             -- dateCheckIn - date
	    NULL, -- dateCheckOut - smalldatetime
	    @idTable,                     -- idTable - int
	    0,                      -- status - int
	    0
		)
END
GO

CREATE PROCEDURE insertBillInfo
@idBill INT, @idFood INT, @quantity INT
AS
BEGIN
	DECLARE @isExistedBillInfo INT;
	DECLARE @foodCount INT = 1;

	SELECT @isExistedBillInfo = id, @foodCount = b.quantity FROM dbo.billInfo AS b 
	WHERE idBill = @idBill AND idFood = @idFood
	IF(@isExistedBillInfo > 0)--nếu có món rồi thì update số lượng
		BEGIN
			DECLARE @newCount INT = @foodCount + @quantity;
			IF(@newCount > 0)
				UPDATE dbo.billInfo SET quantity = @foodCount + @quantity WHERE idFood = @idFood AND idBill = @idBill
			ELSE
				DELETE dbo.billInfo WHERE idBill = @idBill AND idFood = @idFood 
		END
    ELSE -- chưa có thì thêm
		BEGIN
			INSERT INTO dbo.billInfo
			(
				idBill,
				idFood,
				quantity
			)
			VALUES
			( @idBill , @idFood, @quantity)
		END
END
GO

CREATE TRIGGER UpdateBillInfo
ON dbo.billInfo FOR INSERT, UPDATE
AS
BEGIN
	DECLARE @idBill INT
	SELECT @idBill = idBill FROM Inserted

	DECLARE @idTable INT;
	SELECT @idTable = idTable FROM dbo.Bill WHERE id = @idBill AND status = 0

	DECLARE @count INT
	SELECT @count = COUNT(*) FROM dbo.billInfo WHERE idBill = @idBill
	IF(@count > 0)
	UPDATE dbo.tableSeat SET status = N'Có người'  WHERE id = @idTable
	ELSE
    UPDATE dbo.tableSeat SET status = N'Trống'  WHERE id = @idTable
END
GO

CREATE TRIGGER UpdateBill
ON dbo.Bill FOR INSERT, UPDATE
AS
BEGIN
	DECLARE @idBill INT;
	DECLARE @idTable INT;

	SELECT @idBill = Inserted.id FROM Inserted
	SELECT @idTable = idTable FROM dbo.Bill WHERE id = @idBill

	DECLARE @quantity INT = 0;
	SELECT @quantity = COUNT(*) FROM dbo.Bill WHERE idTable = @idTable AND status = 0
	IF(@quantity = 0)
		UPDATE dbo.tableSeat SET status = N'Trống' WHERE id = @idTable
END
GO

-- thực hiện chuyển bàn thông qua lưu Địa chỉ bill
-- lưu idBill bàn mới
--lưu idBill bàn cũ

CREATE PROCEDURE SwitchTable
    @IDTable1 INT,
    @IDTable2 INT
AS
BEGIN
    DECLARE @firstBillID INT;
    DECLARE @secondBillID INT;

    DECLARE @isFirstTableEmpty INT = 1;
    DECLARE @isSecondTableEmpty INT = 1;

    SELECT @firstBillID = id
    FROM dbo.Bill
    WHERE idTable = @IDTable1
          AND status = 0;
    SELECT @secondBillID = id
    FROM dbo.Bill
    WHERE idTable = @IDTable2
          AND status = 0;

    IF (@firstBillID IS NULL)
    BEGIN
        INSERT dbo.Bill
        (
            dateCheckIn,
            dateCheckOut,
            idTable,
            status
        )
        VALUES
        (   GETDATE(), -- dateCheckIn - date
            NULL,      -- dateCheckOut - smalldatetime
            @IDTable1, -- idTable - int
            0          -- status - int
            );
        SELECT @firstBillID = MAX(id)
        FROM dbo.Bill
        WHERE idTable = @IDTable1
              AND status = 0;
    END;

    SELECT @isFirstTableEmpty = COUNT(*)
    FROM dbo.billInfo
    WHERE idBill = @firstBillID;

    IF (@secondBillID IS NULL) -- bug ở đây, so sánh vs null dùng is
    BEGIN
        INSERT dbo.Bill
        (
            dateCheckIn,
            dateCheckOut,
            idTable,
            status
        )
        VALUES
        (   GETDATE(), -- dateCheckIn - date
            NULL,      -- dateCheckOut - smalldatetime
            @IDTable2, -- idTable - int
            0          -- status - int
            );
        SELECT @secondBillID = MAX(id)
        FROM dbo.Bill
        WHERE idTable = @IDTable2
              AND status = 0;
    END;

    SELECT @isSecondTableEmpty = COUNT(*)
    FROM dbo.billInfo
    WHERE idBill = @secondBillID;


    SELECT id
    INTO IDBillInfoTable
    FROM dbo.billInfo
    WHERE idBill = @secondBillID;
    UPDATE dbo.billInfo
    SET idBill = @secondBillID
    WHERE idBill = @firstBillID;
    UPDATE dbo.billInfo
    SET idBill = @firstBillID
    WHERE id IN
          (
              SELECT * FROM dbo.IDBillInfoTable
          );

    DROP TABLE dbo.IDBillInfoTable; -- dùng xong xóa đi để dùng lại

    IF (@isFirstTableEmpty = 0)
        UPDATE dbo.tableSeat
        SET status = N'Trống'
        WHERE id = @IDTable2;
    IF (@isSecondTableEmpty = 0)
        UPDATE dbo.tableSeat
        SET status = N'Trống'
        WHERE id = @IDTable1;
END;
GO

CREATE PROC GetListBillByDate
    @dateCheckIn DATE,
    @dateCheckOut DATE
AS
BEGIN
    SELECT t.name, b.id,
           b.dateCheckIn,
           b.dateCheckOut,
           b.discount,
           b.totalPrice
    FROM dbo.Bill AS b,
         dbo.tableSeat AS t
    WHERE b.dateCheckIn >= @dateCheckIn
          AND b.dateCheckOut <= @dateCheckOut
          AND b.status = 1
          AND t.id = b.idTable;
END;
GO

CREATE PROC UpdateAccount
@userName NVARCHAR(100), @displayName NVARCHAR(100), @password NVARCHAR(100), @newpassword NVARCHAR(100)
AS
BEGIN
	DECLARE @isRightPass INT
	SELECT @isRightPass = COUNT(*) FROM dbo.Account WHERE userName = @userName AND password = @password

	IF(@isRightPass >=1)
	BEGIN
		IF(@newpassword IS NULL AND @newpassword = ' ') -- nếu không nhập mật khẩu mới thì chỉ đổi tên hiển thị
			BEGIN
				UPDATE dbo.Account SET displayName = @displayName WHERE userName = @userName
			END
		ELSE
			BEGIN
				UPDATE dbo.Account SET displayName = @displayName , password = @newpassword WHERE userName = @userName
			END
	END
END
GO

CREATE TRIGGER DeleteBillInfo
ON dbo.BillInfo FOR DELETE
AS 
BEGIN
	DECLARE @idBillInfo INT
	DECLARE @idBill INT
	SELECT @idBillInfo =  id, @idBill = idBill FROM Deleted

	DECLARE @idTable INT
	SELECT @idTable = idTable  FROM dbo.Bill WHERE id = @idBill

	DECLARE @count INT
	SELECT @count = COUNT(*) FROM dbo.billInfo AS bi, dbo.Bill AS b WHERE b.id = bi.idBill AND b.id = @idBill AND b.status = 0

	IF(@count = 0)
	BEGIN
		UPDATE dbo.tableSeat SET status = N'Trống' WHERE id = @idTable
	END
    
END
GO

CREATE PROC InsertAccount
@userName NVARCHAR(100), @displayName NVARCHAR(100) , @pass NVARCHAR(100) , @type INT
AS
BEGIN
  DECLARE @isExistDisplayName INT = 0
  SELECT @isExistDisplayName = COUNT(*) FROM dbo.Account WHERE displayName = @displayName
  IF(@isExistDisplayName = 0)
  BEGIN
	INSERT dbo.Account
	(
	    userName,
	    displayName,
	    password,
	    accountType
	)
	VALUES
	( @userName, @displayName , @pass, @type)
  END
  
END
GO

-- 1page 2 dòng hóa đơn
-- pageCount = 2 số dòng 1 trang
-- pageNum = 2 trang hiện tại
-- đây là cthuc tính số dòng / page hiện tại

--SELECT TOP pageCount * pageNum FROM dbo.Bill
--EXCEPT
--SELECT TOP pageCount FROM dbo.Bill

CREATE PROC GetListBillByDateAndPage
    @dateCheckIn DATE,
    @dateCheckOut DATE,
	@page INT
AS
BEGIN
	DECLARE @pageRows INT = 10
	DECLARE @selectRows INT = @pageRows-- so dong lay ra
	DECLARE @exceptRows INT = (@page - 1) * @pageRows

    ;WITH BillShow AS (SELECT b.id , 
			t.name,
           b.dateCheckIn,
           b.dateCheckOut,
           b.discount,
           b.totalPrice
    FROM dbo.Bill AS b,
         dbo.tableSeat AS t
    WHERE b.dateCheckIn >= @dateCheckIn
          AND b.dateCheckOut <= @dateCheckOut
          AND b.status = 1
          AND t.id = b.idTable)

	SELECT TOP (@selectRows) * FROM BillShow WHERE BillShow.id NOT IN (SELECT TOP (@exceptRows) id FROM BillShow)

END
GO

CREATE PROC GetNumBillByDate
    @dateCheckIn DATE,
    @dateCheckOut DATE
AS
BEGIN
    SELECT COUNT(*)
    FROM dbo.Bill AS b,
         dbo.tableSeat AS t
    WHERE b.dateCheckIn >= @dateCheckIn
          AND b.dateCheckOut <= @dateCheckOut
          AND b.status = 1
          AND t.id = b.idTable;
END;
GO
