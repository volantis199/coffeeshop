﻿using CoffeeShop.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace CoffeeShop.Data_Access_Layer
{
    public class BillAccess
    {
        private BillAccess() { }
        private static BillAccess instance;
        public static BillAccess Instance
        {
            get
            {
                if (instance == null)
                    instance = new BillAccess();
                return BillAccess.instance;
            }

            private set
            {
                instance = value;
            }
        }

        public int GetUncheckedBillIdByTableId(int id)
        {
            /// Thành công:  return bill Id
            /// Thất bại: -1
            DataTable data = DataAccess.Instance.executeQuery("SELECT * FROM dbo.Bill WHERE idTable = " + id + " and status = 0");
            if (data.Rows.Count > 0)
            {
                Bill bill = new Bill(data.Rows[0]);
                return bill.Id;
            }
            else return -1;
        }

        public void InsertBill(int idTable)
        {
            /// Bill gồm 4 trường là dateCheckIn, dateCheckOut, idTable và status(đã thanh toán hay chưa)
            /// thì rõ ràng thêm bill chỉ cần thêm 1 trường duy nhất là idTable do dateCheckIn là ngày hiện tại
            ///dateCheckOut = Null do status = 0 vì chưa thanh toán
            ///nên chỉ cần thêm vào idTable
            DataAccess.Instance.executeNonQuery("EXEC InsertBill @idTable", new object[] { idTable });
        }

        public int GetMaxIdBill()
        {
            try
            {
                return (int)DataAccess.Instance.executeScalar("Select max(id) from dbo.Bill");

            }
            catch 
            {
                return 1;
            }
        }

        public void ChekOut(int id, int discount, float totalPrice)
        {
            string query = "UPDATE dbo.Bill SET dateCheckOut = GETDATE(), status = 1," + " discount = " + discount + ", totalPrice = " + totalPrice +" WHERE id = " + id;
            DataAccess.Instance.executeNonQuery(query);
        }

        public DataTable GetListBillByDate(DateTime date1, DateTime date2)
        {
           return DataAccess.Instance.executeQuery("exec GetListBillByDate @dateCheckIn , @dateCheckOut", new object[] { date1, date2 });
        }

        public DataTable GetListBillByDateAndPage(DateTime date1, DateTime date2, int pageNum)
        {
            return DataAccess.Instance.executeQuery("exec GetListBillByDateAndPage @dateCheckIn , @dateCheckOut , @page", new object[] { date1, date2, pageNum });
        }
        
        //ham lay so bill hien tai dang co de chia page
        public int GetNumBillByDate(DateTime date1, DateTime date2)
        {
            return (int)DataAccess.Instance.executeScalar("exec GetNumBillByDate @dateCheckIn , @dateCheckOut", new object[] { date1, date2 });
        }
    }
}
