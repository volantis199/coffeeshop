﻿using CoffeeShop.Entity;
using System.Collections.Generic;
using System.Data;
using System;
using System.Security.Cryptography;
using System.Text;
using System.Linq;

namespace CoffeeShop.Data_Access_Layer
{
    public class AccountAccess
    {
        private AccountAccess() { }

        private static AccountAccess instance;
        public static AccountAccess Instance
        {
            get
            {
                if(instance == null)
                {
                    instance = new AccountAccess();
                }
                return instance;
            }

            private set
            {
               instance = value;
            }
        }

        public bool login(string userName, string password)
        {
            //add MD5 mã hóa mật khẩu
            //byte[] tmp = ASCIIEncoding.ASCII.GetBytes(password);
            //chuyển pass thành mảng bytes
            //byte[] hashData = new MD5CryptoServiceProvider().ComputeHash(tmp);
            //dữ liệu chia nhỏ ra

            //đảo dữ liệu ngược tránh dò mật khẩu
            //var list = hashData.ToString();
            //list.Reverse();
            //nếu dùng đưa list thay pass

            //string hassPass = " ";
            //foreach (byte item in hashData)
            //{
            //    hassPass += item;
            //}

            string query = "UserLogin @userName , @password ";
            DataTable result = DataAccess.Instance.executeQuery(query, new object[] {userName, password/*hassPass*/ /*list*/});
            return result.Rows.Count > 0;
        }

        public Account GetAccountByUserName(string userName)
        {
            DataTable data = DataAccess.Instance.executeQuery("select * from Account where userName = '" + userName + "'");
            foreach (DataRow item in data.Rows)
            {
                return new Account(item);
            }

            return null;
        }

        public Account getAccountTypeByUserName(string userName)
        {
            Account acc = null;

            string query = "SELECT* FROM dbo.Account where username = " + userName;
            DataTable data = DataAccess.Instance.executeQuery(query);
            foreach (DataRow item in data.Rows)
            {
                acc = new Account(item);
                return acc;
            }

            return acc;
        }

        public List<Account> getListAccount()
        {
            List<Account> list = new List<Account>();
            string query = "SELECT * FROM dbo.Account";
            DataTable data = DataAccess.Instance.executeQuery(query);
            foreach (DataRow item in data.Rows)
            {
                Account acc = new Account(item);
                list.Add(acc);
            }

            return list;
        }

        public bool UpdateAccount(string userName, string displayName, string pass, string newpass)
        {
            int result = DataAccess.Instance.executeNonQuery("exec UpdateAccount @userName , @displayName , @password , @newpassword", new object[] { userName, displayName, pass, newpass });

            return result > 0; //xem có hàng nào có update đc k
        }
        public bool InsertAccount(string userName, string displayName, string pass, int type)
        {
            int result = DataAccess.Instance.executeNonQuery("exec InsertAccount @userName , @displayName , @pass , @type", new object[] { userName, displayName, pass, type });

            return result > 0;
        }

        public bool ResetPassword(string userName)
        {
            string query = string.Format("Update dbo.Account set password = '1' where userName = N'{0}'", userName);
            int result = DataAccess.Instance.executeNonQuery(query);

            return result > 0;
        }

        public bool DeleteAccount(string userName)
        {
            string query = string.Format("Delete dbo.Account where userName = N'{0}'", userName);
            int result = DataAccess.Instance.executeNonQuery(query);

            return result > 0;
        }

        public bool EditAccount(string userName, string displayName, int type)
        {
            string query = string.Format("Update dbo.Account set displayName = N'{0}' ,accountType = {1} where userName = N'{2}'", displayName, type, userName);

            int result = DataAccess.Instance.executeNonQuery(query);
            return result > 0;
        }

        
    }
}
