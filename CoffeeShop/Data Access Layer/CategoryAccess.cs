﻿using CoffeeShop.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace CoffeeShop.Data_Access_Layer
{
    public class CategoryAccess
    {
        private CategoryAccess() { }
        private static CategoryAccess instance;

        public static CategoryAccess Instance
        {
            get
            {
                if (instance == null)
                    instance = new CategoryAccess();
                return CategoryAccess.instance;
            }

            private set
            {
                instance = value;
            }
        }

        public List<Category> getListCategory()
        {
            List<Category> listCategory = new List<Category>();

            string query = "SELECT* FROM dbo.foodCategory";
            DataTable data = DataAccess.Instance.executeQuery(query);
            foreach (DataRow item in data.Rows)
            {
                Category category = new Category(item);
                listCategory.Add(category);
            }

            return listCategory;
        }

        public Category getCategoryByID(int id)
        {
            Category cate = null;

            string query = "SELECT* FROM dbo.foodCategory where id = " + id;
            DataTable data = DataAccess.Instance.executeQuery(query);
            foreach (DataRow item in data.Rows)
            {
                cate = new Category(item);
                return cate;
            }

            return cate;
        }

        public bool insertCategory(string name)
        {
            string query = "INSERT dbo.foodCategory(name) VALUES(N'" + name + "')";
            int result = DataAccess.Instance.executeNonQuery(query);

            return result > 0;
        }

        public bool updateCategory(int id, string name)
        {
            string query = string.Format("UPDATE dbo.foodCategory SET name =  N'{0}'WHERE id = {1}", name, id);
            int result = DataAccess.Instance.executeNonQuery(query);

            return result > 0;
        }

        public bool deleteCategory(int id)
        {
            string q1 = "select id from dbo.Food where idCategory = " + id;

            DataTable data = DataAccess.Instance.executeQuery(q1);
            foreach (DataRow item in data.Rows)
            {
                int idFood = (int)(item["id"]);
                BillInfoAccess.Instance.deleteBillInfoByIdFood(idFood);
            }
            string q2 = string.Format("delete dbo.Food where idCategory = {0}", id);
            DataAccess.Instance.executeNonQuery(q2);
            string query = string.Format("delete dbo.foodCategory where id = {0}", id);
            int result = DataAccess.Instance.executeNonQuery(query);

            return result > 0;
        }
    }
}
