﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace CoffeeShop.Data_Access_Layer
{
    public class DataAccess
    {
        private static SqlConnection sqlCon;

        private static DataAccess instance;
        public static DataAccess Instance
        {
            get
            {
                if(instance == null)
                {
                    instance = new DataAccess();
                }
                return instance;
            }

            private set
            {
                instance = value;
            }
        }

        private DataAccess() {}
        private void OpenConnection()
        {
            string connection = @"Data Source=DESKTOP-PQGL9RC\SQLEXPRESS;Initial Catalog=CoffeeShop;Integrated Security=True";
            sqlCon = new SqlConnection(connection);
            try
            {
                sqlCon.Open();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private static void CloseConnection()
        {
            //if open so close
            if (sqlCon.State == ConnectionState.Open)
                sqlCon.Close();
        }

        public DataTable executeQuery(string query , object[] parametters = null)
        {
            DataTable table = new DataTable();
            //try
            //{
                OpenConnection();
                SqlCommand cmd = new SqlCommand(query, sqlCon);
                int i = 0;
                if(parametters != null)
                {
                    string[] listPara = query.Split(' ');
                    foreach (var item in listPara)
                    {
                        if(item.Contains('@'))
                        {
                            cmd.Parameters.AddWithValue(item, parametters[i]);
                            ++i;
                        }
                    }
                }

                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(table);
                CloseConnection();
            //}
            //catch (Exception ex)
            //{
            //    throw ex;
            //}
            return table;
        }
        public int executeNonQuery(string query, object[] parametters = null)
        {
            int line = 0;//lines affected
            try
            {
                OpenConnection();
                SqlCommand cmd = new SqlCommand(query, sqlCon);
                int i = 0;
                if (parametters != null)
                {
                    string[] listPara = query.Split(' ');
                    foreach (var item in listPara)
                    {
                        if (item.Contains('@'))
                        {
                            cmd.Parameters.AddWithValue(item, parametters[i]);
                            ++i;
                        }
                    }
                }
                line = cmd.ExecuteNonQuery();
                CloseConnection();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return line;
        }

        public object executeScalar(string query, object[] parametters = null)
        {
            object ob = 0;
            try
            {
                OpenConnection();
                SqlCommand cmd = new SqlCommand(query, sqlCon);
                int i = 0;
                if (parametters != null)
                {
                    string[] listPara = query.Split(' ');
                    foreach (var item in listPara)
                    {
                        if (item.Contains('@'))
                        {
                            cmd.Parameters.AddWithValue(item, parametters[i]);
                            ++i;
                        }
                    }
                }
                ob = cmd.ExecuteScalar();
                CloseConnection();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ob;
        }
    }
}
