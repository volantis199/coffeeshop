﻿using CoffeeShop.Entity;
using System.Collections.Generic;
using System.Data;

namespace CoffeeShop.Data_Access_Layer
{
    public class FoodAccess
    {
        private FoodAccess() { }
        private static FoodAccess instance;

        public static FoodAccess Instance
        {
            get
            {
                if (instance == null)
                    instance = new FoodAccess();
                return FoodAccess.instance;
            }

            private set
            {
                instance = value;
            }
        }

        public List<Food> GetFoodListByCategoryID(int idCategory)
        {
            List<Food> list = new List<Food>();

            string query = "SELECT * FROM dbo.Food WHERE idCategory = " + idCategory;
            DataTable data = DataAccess.Instance.executeQuery(query);
            foreach (DataRow item in data.Rows)
            {
                Food food = new Food(item);
                list.Add(food);
            }

            return list;
        }

        public List<Food> GetFoodList()
        {
            List<Food> list = new List<Food>();

            string query = "SELECT * FROM dbo.Food";
            DataTable data = DataAccess.Instance.executeQuery(query);
            foreach (DataRow item in data.Rows)
            {
                Food food = new Food(item);
                list.Add(food);
            }

            return list;
        }

        public List<Food> GetFoodListByName(string name)
        {
            List<Food> list = new List<Food>();

            string query = string.Format("SELECT * FROM dbo.Food where name like N'%{0}%'", name);
            DataTable data = DataAccess.Instance.executeQuery(query);
            foreach (DataRow item in data.Rows)
            {
                Food food = new Food(item);
                list.Add(food);
            }

            return list;
        }

        public bool InsertFood(string name, int idCategory, float price)
        {
            string query = "INSERT dbo.Food(name, idCategory, price) VALUES( N'" + name+ "'," + idCategory +  ", " + price + ")";
            int result = DataAccess.Instance.executeNonQuery(query);

            return result > 0; 
        }

        public bool UpdateFood(int id, string name, int idCategory, float price)
        {
            string query = string.Format("UPDATE dbo.Food SET name =  N'{0}', idCategory =  {1}, price = {2} WHERE id = {3}", name, idCategory, price, id);
            int result = DataAccess.Instance.executeNonQuery(query);

            return result > 0;
        }

        public bool DeleteFood(int idFood)
        {
            BillInfoAccess.Instance.deleteBillInfoByIdFood(idFood);
            string query = string.Format("delete dbo.Food where id = {0}", idFood);
            int result = DataAccess.Instance.executeNonQuery(query);

            return result > 0;
        }
    }
}
