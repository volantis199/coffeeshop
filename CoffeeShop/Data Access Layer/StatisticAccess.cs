﻿using CoffeeShop.Entity;
using System.Collections.Generic;
using System.Data;

namespace CoffeeShop.Data_Access_Layer
{
    public class StatisticAccess
    {
        private static StatisticAccess instance;

        private StatisticAccess() { }

        public static StatisticAccess Instance
        {
            get
            {
                if (instance == null)
                    instance = new StatisticAccess();
                return StatisticAccess.instance;
            }

            private set
            {
                instance = value;
            }
        }

        public List<Statistic> GetListStatisticByTable(int idTable)
        {
            List<Statistic> list = new List<Statistic>();

            string query = @"SELECT f.name, bi.quantity, f.price, f.price*bi.quantity AS totalPrice FROM dbo.billInfo AS bi, dbo.Bill AS b, dbo.Food AS f
            WHERE bi.idBill = b.id AND bi.idFood = f.id AND b.status = 0 and b.idTable = " + idTable ;

            DataTable data = DataAccess.Instance.executeQuery(query);

            foreach (DataRow item in data.Rows)
            {
                Statistic statis = new Statistic(item);
                list.Add(statis);
            }
            return list;
        }
    }
}
