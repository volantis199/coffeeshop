﻿using CoffeeShop.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace CoffeeShop.Data_Access_Layer
{
    public class TableAccess
    {
        private TableAccess() { }
        private static TableAccess instance;
        public static TableAccess Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new TableAccess();
                }
                return instance;
            }

            private set
            {
                instance = value;
            }
        }

        public static int tableWidth = 100;
        public static int tableHeight = 100;

        public List<Table> LoadTableList()
        {
            List<Table> tableList = new List<Table>();

            DataTable data = DataAccess.Instance.executeQuery("GetTableList");
            foreach (DataRow item in data.Rows)
            {
                Table table = new Table(item);
                tableList.Add(table);
            }
            return tableList;
        }

        public void SwitchTable(int idTable1, int idTable2)
        {
            DataAccess.Instance.executeQuery("SwitchTable @idTable1 , @idTable2", new object[] { idTable1, idTable2 });
        }

        public List<Table> GetListTable()
        {
            List<Table> list = new List<Table>();

            string query = "SELECT * FROM dbo.tableSeat";
            DataTable data = DataAccess.Instance.executeQuery(query);
            foreach (DataRow item in data.Rows)
            {
                Table table = new Table(item);
                list.Add(table);
            }

            return list;
        }

        public Table GetTableStatusByID(int id)
        {
            Table table = null;

            string query = "SELECT* FROM dbo.tableSeat where id = " + id;
            DataTable data = DataAccess.Instance.executeQuery(query);
            foreach (DataRow item in data.Rows)
            {
                table = new Table(item);
                return table;
            }

            return table;
        }

        public bool InsertTable(string name, string status)
        {
            string query = string.Format("INSERT dbo.tableSeat( name, status) VALUES( N'{0}' , N'{1}')", name, status);
            int result = DataAccess.Instance.executeNonQuery(query);

            return result > 0;
        }

        public bool DeleteTable(int id)
        {
            string q1 = "select * from dbo.Bill where idTable = " + id;
            DataTable data = DataAccess.Instance.executeQuery(q1);
            foreach (DataRow item in data.Rows)
            {
                int idBill= (int)(item["id"]);
                BillInfoAccess.Instance.deleteBillInfoByIdBill(idBill);
            }
            string q2 = "delete dbo.Bill where idTable = " + id;
            DataAccess.Instance.executeNonQuery(q2);

            string query = "delete dbo.tableSeat where id = " + id;
            int result = DataAccess.Instance.executeNonQuery(query);
            return result > 0;
        }

        public bool UpdateTable(int id, string name, string status)
        {
            //string q1 = "select * from dbo.Bill where status = 0 and idTable = " + id;
            //DataTable data = DataAccess.Instance.executeQuery(q1);
            //foreach (DataRow item in data.Rows)
            //{
            //    int idBill = (int)(item["id"]);
            //    BillInfoAccess.Instance.deleteBillInfoByIdBill(idBill);
            //}
            //string q2 = "delete dbo.Bill where status = 0 and idTable = " + id;
            //DataAccess.Instance.executeNonQuery(q2);

            string query = string.Format("UPDATE dbo.tableSeat SET name =  N'{0}' , status = N'{1}' WHERE id = {2}", name, status, id);
            int result = DataAccess.Instance.executeNonQuery(query);

            return result > 0;
        }
    }
}
