﻿using CoffeeShop.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace CoffeeShop.Data_Access_Layer
{
    public class BillInfoAccess
    {
        private BillInfoAccess() { }
        private static BillInfoAccess instance;
        public static BillInfoAccess Instance
        {
            get
            {
                if (instance == null)
                    instance = new BillInfoAccess();
                return BillInfoAccess.instance;
            }

            private set
            {
                instance = value;
            }
        }

        public List<BillInfo> getListBillInfo(int idBill)
        {
            List<BillInfo> list = new List<BillInfo>();

            DataTable data = DataAccess.Instance.executeQuery("select * from dbo.billInfo where idBill = " + idBill);

            foreach (DataRow item in data.Rows)
            {
                BillInfo bill = new BillInfo(item);
                list.Add(bill);
            }

            return list;
        }

        public void insertBillInfo(int idBill, int idFood, int quantity)
        {

            DataAccess.Instance.executeNonQuery("insertBillInfo @idBill , @idFood , @quantity", new object[] { idBill, idFood, quantity});
        }

        public void deleteBillInfoByIdFood(int id)
        {
            DataAccess.Instance.executeQuery("DELETE dbo.BillInfo where idFood = " + id);
        }

        public void deleteBillInfoByIdBill(int id)
        {
            DataAccess.Instance.executeQuery("DELETE dbo.BillInfo where idBill = " + id);
        }
    }
}
