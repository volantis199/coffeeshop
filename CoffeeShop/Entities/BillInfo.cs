﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace CoffeeShop.Entity
{
    public class BillInfo
    {
        private int id;
        private int idBill;
        private int idFood;
        private int quantity;

        public BillInfo() { }

        public BillInfo(DataRow row)
        {
            this.Id = (int)row["id"];
            this.IdBill = (int)row["idBill"];
            this.IdFood = (int)row["idFood"];
            this.Quantity = (int)row["quantity"];
        }

        public BillInfo(int id, int idBill, int idFood, int quantity)
        {
            this.Id = id;
            this.IdBill = idBill;
            this.IdFood = idFood;
            this.Quantity = quantity;
        }
        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public int IdBill
        {
            get
            {
                return idBill;
            }

            set
            {
                idBill = value;
            }
        }

        public int IdFood
        {
            get
            {
                return idFood;
            }

            set
            {
                idFood = value;
            }
        }

        public int Quantity
        {
            get
            {
                return quantity;
            }

            set
            {
                quantity = value;
            }
        }
    }
}
