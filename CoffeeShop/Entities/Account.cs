﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace CoffeeShop.Entity
{
    public class Account
    {
        public Account(string userName, string displayName, int type, string password = null)
        {
            UserName = userName;
            Displayname = displayName;
            Type = type;
            Password = password;
        }

        public Account(DataRow row)
        {
            UserName = row["userName"].ToString();
            Displayname = row["displayName"].ToString();
            Type = (int)row["accountType"];
            Password = row["password"].ToString();
        }

        private string userName;
        private string password;
        private string displayname;
        private int type;

        public string UserName
        {
            get
            {
                return userName;
            }

            set
            {
                userName = value;
            }
        }

        public string Password
        {
            get
            {
                return password;
            }

            set
            {
                password = value;
            }
        }

        public int Type
        {
            get
            {
                return type;
            }

            set
            {
                type = value;
            }
        }

        public string Displayname
        {
            get
            {
                return displayname;
            }

            set
            {
                displayname = value;
            }
        }
    }
}
