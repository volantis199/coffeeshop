USE [master]
GO
/****** Object:  Database [CoffeeShop]    Script Date: 8/8/2019 9:07:03 AM ******/
CREATE DATABASE [CoffeeShop]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'CoffeeShop', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\CoffeeShop.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'CoffeeShop_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\CoffeeShop_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [CoffeeShop] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [CoffeeShop].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [CoffeeShop] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [CoffeeShop] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [CoffeeShop] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [CoffeeShop] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [CoffeeShop] SET ARITHABORT OFF 
GO
ALTER DATABASE [CoffeeShop] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [CoffeeShop] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [CoffeeShop] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [CoffeeShop] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [CoffeeShop] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [CoffeeShop] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [CoffeeShop] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [CoffeeShop] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [CoffeeShop] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [CoffeeShop] SET  ENABLE_BROKER 
GO
ALTER DATABASE [CoffeeShop] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [CoffeeShop] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [CoffeeShop] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [CoffeeShop] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [CoffeeShop] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [CoffeeShop] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [CoffeeShop] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [CoffeeShop] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [CoffeeShop] SET  MULTI_USER 
GO
ALTER DATABASE [CoffeeShop] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [CoffeeShop] SET DB_CHAINING OFF 
GO
ALTER DATABASE [CoffeeShop] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [CoffeeShop] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [CoffeeShop] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [CoffeeShop] SET QUERY_STORE = OFF
GO
USE [CoffeeShop]
GO
/****** Object:  Table [dbo].[Account]    Script Date: 8/8/2019 9:07:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Account](
	[userName] [nvarchar](100) NOT NULL,
	[displayName] [nvarchar](100) NOT NULL,
	[password] [nvarchar](1000) NOT NULL,
	[accountType] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[userName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Bill]    Script Date: 8/8/2019 9:07:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Bill](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[dateCheckIn] [date] NOT NULL,
	[dateCheckOut] [smalldatetime] NULL,
	[idTable] [int] NOT NULL,
	[status] [int] NOT NULL,
	[discount] [int] NULL,
	[totalPrice] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[billInfo]    Script Date: 8/8/2019 9:07:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[billInfo](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idBill] [int] NOT NULL,
	[idFood] [int] NOT NULL,
	[quantity] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Food]    Script Date: 8/8/2019 9:07:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Food](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](30) NOT NULL,
	[idCategory] [int] NOT NULL,
	[price] [float] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[foodCategory]    Script Date: 8/8/2019 9:07:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[foodCategory](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](30) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tableSeat]    Script Date: 8/8/2019 9:07:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tableSeat](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](100) NOT NULL,
	[status] [nvarchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Account] ([userName], [displayName], [password], [accountType]) VALUES (N'admin', N'Ad', N'admin', 1)
INSERT [dbo].[Account] ([userName], [displayName], [password], [accountType]) VALUES (N'staff', N'staff', N'staff', 0)
SET IDENTITY_INSERT [dbo].[Food] ON 

INSERT [dbo].[Food] ([id], [name], [idCategory], [price]) VALUES (1, N'Cà phê sữa đá', 1, 25000)
INSERT [dbo].[Food] ([id], [name], [idCategory], [price]) VALUES (2, N'Cà phê đen', 1, 20000)
INSERT [dbo].[Food] ([id], [name], [idCategory], [price]) VALUES (3, N'Bạc xỉu', 1, 25000)
INSERT [dbo].[Food] ([id], [name], [idCategory], [price]) VALUES (4, N'Hướng dương', 2, 10000)
INSERT [dbo].[Food] ([id], [name], [idCategory], [price]) VALUES (5, N'Poca', 2, 6000)
INSERT [dbo].[Food] ([id], [name], [idCategory], [price]) VALUES (6, N'Mousse', 3, 30000)
INSERT [dbo].[Food] ([id], [name], [idCategory], [price]) VALUES (7, N'Bông lan trứng muối', 3, 45000)
INSERT [dbo].[Food] ([id], [name], [idCategory], [price]) VALUES (8, N'Gin & Tonic', 4, 50000)
SET IDENTITY_INSERT [dbo].[Food] OFF
SET IDENTITY_INSERT [dbo].[foodCategory] ON 

INSERT [dbo].[foodCategory] ([id], [name]) VALUES (1, N'Cà phê')
INSERT [dbo].[foodCategory] ([id], [name]) VALUES (2, N'Snack')
INSERT [dbo].[foodCategory] ([id], [name]) VALUES (3, N'Bánh')
INSERT [dbo].[foodCategory] ([id], [name]) VALUES (4, N'Cocktail')
SET IDENTITY_INSERT [dbo].[foodCategory] OFF
SET IDENTITY_INSERT [dbo].[tableSeat] ON 

INSERT [dbo].[tableSeat] ([id], [name], [status]) VALUES (1, N'Bàn 1', N'Trống')
INSERT [dbo].[tableSeat] ([id], [name], [status]) VALUES (2, N'Bàn 2', N'Trống')
INSERT [dbo].[tableSeat] ([id], [name], [status]) VALUES (3, N'Bàn 3', N'Trống')
INSERT [dbo].[tableSeat] ([id], [name], [status]) VALUES (4, N'Bàn 4', N'Trống')
INSERT [dbo].[tableSeat] ([id], [name], [status]) VALUES (5, N'Bàn 5', N'Trống')
INSERT [dbo].[tableSeat] ([id], [name], [status]) VALUES (6, N'Bàn 6', N'Trống')
INSERT [dbo].[tableSeat] ([id], [name], [status]) VALUES (7, N'Bàn 7', N'Trống')
INSERT [dbo].[tableSeat] ([id], [name], [status]) VALUES (8, N'Bàn 8', N'Trống')
INSERT [dbo].[tableSeat] ([id], [name], [status]) VALUES (9, N'Bàn 9', N'Trống')
INSERT [dbo].[tableSeat] ([id], [name], [status]) VALUES (10, N'Bàn 10', N'Trống')
INSERT [dbo].[tableSeat] ([id], [name], [status]) VALUES (11, N'Bàn 11', N'Trống')
INSERT [dbo].[tableSeat] ([id], [name], [status]) VALUES (12, N'Bàn 12', N'Trống')
SET IDENTITY_INSERT [dbo].[tableSeat] OFF
ALTER TABLE [dbo].[Account] ADD  DEFAULT (N'user') FOR [displayName]
GO
ALTER TABLE [dbo].[Account] ADD  DEFAULT ((0)) FOR [password]
GO
ALTER TABLE [dbo].[Account] ADD  DEFAULT ((0)) FOR [accountType]
GO
ALTER TABLE [dbo].[Bill] ADD  DEFAULT (getdate()) FOR [dateCheckIn]
GO
ALTER TABLE [dbo].[Bill] ADD  DEFAULT ((0)) FOR [status]
GO
ALTER TABLE [dbo].[billInfo] ADD  DEFAULT ((0)) FOR [quantity]
GO
ALTER TABLE [dbo].[Food] ADD  DEFAULT (N'chưa đặt tên') FOR [name]
GO
ALTER TABLE [dbo].[foodCategory] ADD  DEFAULT (N'chưa đặt tên') FOR [name]
GO
ALTER TABLE [dbo].[tableSeat] ADD  DEFAULT (N'Trống') FOR [status]
GO
ALTER TABLE [dbo].[Bill]  WITH CHECK ADD FOREIGN KEY([idTable])
REFERENCES [dbo].[tableSeat] ([id])
GO
ALTER TABLE [dbo].[billInfo]  WITH CHECK ADD FOREIGN KEY([idFood])
REFERENCES [dbo].[Food] ([id])
GO
ALTER TABLE [dbo].[billInfo]  WITH CHECK ADD FOREIGN KEY([idBill])
REFERENCES [dbo].[Bill] ([id])
GO
ALTER TABLE [dbo].[Food]  WITH CHECK ADD FOREIGN KEY([idCategory])
REFERENCES [dbo].[foodCategory] ([id])
GO
/****** Object:  StoredProcedure [dbo].[GetListBillByDate]    Script Date: 8/8/2019 9:07:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[GetListBillByDate]
    @dateCheckIn DATE,
    @dateCheckOut DATE
AS
BEGIN
    SELECT t.name, b.id,
           b.dateCheckIn,
           b.dateCheckOut,
           b.discount,
           b.totalPrice
    FROM dbo.Bill AS b,
         dbo.tableSeat AS t
    WHERE b.dateCheckIn >= @dateCheckIn
          AND b.dateCheckOut <= @dateCheckOut
          AND b.status = 1
          AND t.id = b.idTable;
END;
GO
/****** Object:  StoredProcedure [dbo].[GetListBillByDateAndPage]    Script Date: 8/8/2019 9:07:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- 1page 2 dòng hóa đơn
-- pageCount = 2 số dòng 1 trang
-- pageNum = 2 trang hiện tại
-- đây là cthuc tính số dòng / page hiện tại

--SELECT TOP pageCount * pageNum FROM dbo.Bill
--EXCEPT
--SELECT TOP pageCount FROM dbo.Bill

CREATE PROC [dbo].[GetListBillByDateAndPage]
    @dateCheckIn DATE,
    @dateCheckOut DATE,
	@page INT
AS
BEGIN
	DECLARE @pageRows INT = 10
	DECLARE @selectRows INT = @pageRows-- so dong lay ra
	DECLARE @exceptRows INT = (@page - 1) * @pageRows

    ;WITH BillShow AS (SELECT b.id , 
			t.name,
           b.dateCheckIn,
           b.dateCheckOut,
           b.discount,
           b.totalPrice
    FROM dbo.Bill AS b,
         dbo.tableSeat AS t
    WHERE b.dateCheckIn >= @dateCheckIn
          AND b.dateCheckOut <= @dateCheckOut
          AND b.status = 1
          AND t.id = b.idTable)

	SELECT TOP (@selectRows) * FROM BillShow WHERE BillShow.id NOT IN (SELECT TOP (@exceptRows) id FROM BillShow)

END
GO
/****** Object:  StoredProcedure [dbo].[GetNumBillByDate]    Script Date: 8/8/2019 9:07:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[GetNumBillByDate]
    @dateCheckIn DATE,
    @dateCheckOut DATE
AS
BEGIN
    SELECT COUNT(*)
    FROM dbo.Bill AS b,
         dbo.tableSeat AS t
    WHERE b.dateCheckIn >= @dateCheckIn
          AND b.dateCheckOut <= @dateCheckOut
          AND b.status = 1
          AND t.id = b.idTable;
END;
GO
/****** Object:  StoredProcedure [dbo].[GetTableList]    Script Date: 8/8/2019 9:07:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetTableList]
AS
BEGIN
	SELECT* FROM dbo.tableSeat
END
GO
/****** Object:  StoredProcedure [dbo].[InsertAccount]    Script Date: 8/8/2019 9:07:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[InsertAccount]
@userName NVARCHAR(100), @displayName NVARCHAR(100) , @pass NVARCHAR(100) , @type INT
AS
BEGIN
  DECLARE @isExistDisplayName INT = 0
  SELECT @isExistDisplayName = COUNT(*) FROM dbo.Account WHERE displayName = @displayName
  IF(@isExistDisplayName = 0)
  BEGIN
	INSERT dbo.Account
	(
	    userName,
	    displayName,
	    password,
	    accountType
	)
	VALUES
	( @userName, @displayName , @pass, @type)
  END
  
END
GO
/****** Object:  StoredProcedure [dbo].[InsertBill]    Script Date: 8/8/2019 9:07:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--tạo stored procedure để thêm Bill
CREATE PROC [dbo].[InsertBill]
@idTable Int
AS
BEGIN
	INSERT INTO dbo.Bill
	(
	    dateCheckIn,
	    dateCheckOut,
	    idTable,
	    status,
		discount
	)
	VALUES
	(   GETDATE(),             -- dateCheckIn - date
	    NULL, -- dateCheckOut - smalldatetime
	    @idTable,                     -- idTable - int
	    0,                      -- status - int
	    0
		)
END
GO
/****** Object:  StoredProcedure [dbo].[insertBillInfo]    Script Date: 8/8/2019 9:07:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[insertBillInfo]
@idBill INT, @idFood INT, @quantity INT
AS
BEGIN
	DECLARE @isExistedBillInfo INT;
	DECLARE @foodCount INT = 1;

	SELECT @isExistedBillInfo = id, @foodCount = b.quantity FROM dbo.billInfo AS b 
	WHERE idBill = @idBill AND idFood = @idFood
	IF(@isExistedBillInfo > 0)--nếu có món rồi thì update số lượng
		BEGIN
			DECLARE @newCount INT = @foodCount + @quantity;
			IF(@newCount > 0)
				UPDATE dbo.billInfo SET quantity = @foodCount + @quantity WHERE idFood = @idFood AND idBill = @idBill
			ELSE
				DELETE dbo.billInfo WHERE idBill = @idBill AND idFood = @idFood 
		END
    ELSE -- chưa có thì thêm
		BEGIN
			INSERT INTO dbo.billInfo
			(
				idBill,
				idFood,
				quantity
			)
			VALUES
			( @idBill , @idFood, @quantity)
		END
END
GO
/****** Object:  StoredProcedure [dbo].[SwitchTable]    Script Date: 8/8/2019 9:07:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- thực hiện chuyển bàn thông qua lưu Địa chỉ bill
-- lưu idBill bàn mới
--lưu idBill bàn cũ

CREATE PROCEDURE [dbo].[SwitchTable]
    @IDTable1 INT,
    @IDTable2 INT
AS
BEGIN
    DECLARE @firstBillID INT;
    DECLARE @secondBillID INT;

    DECLARE @isFirstTableEmpty INT = 1;
    DECLARE @isSecondTableEmpty INT = 1;

    SELECT @firstBillID = id
    FROM dbo.Bill
    WHERE idTable = @IDTable1
          AND status = 0;
    SELECT @secondBillID = id
    FROM dbo.Bill
    WHERE idTable = @IDTable2
          AND status = 0;

    IF (@firstBillID IS NULL)
    BEGIN
        INSERT dbo.Bill
        (
            dateCheckIn,
            dateCheckOut,
            idTable,
            status
        )
        VALUES
        (   GETDATE(), -- dateCheckIn - date
            NULL,      -- dateCheckOut - smalldatetime
            @IDTable1, -- idTable - int
            0          -- status - int
            );
        SELECT @firstBillID = MAX(id)
        FROM dbo.Bill
        WHERE idTable = @IDTable1
              AND status = 0;
    END;

    SELECT @isFirstTableEmpty = COUNT(*)
    FROM dbo.billInfo
    WHERE idBill = @firstBillID;

    IF (@secondBillID IS NULL) -- bug ở đây, so sánh vs null dùng is
    BEGIN
        INSERT dbo.Bill
        (
            dateCheckIn,
            dateCheckOut,
            idTable,
            status
        )
        VALUES
        (   GETDATE(), -- dateCheckIn - date
            NULL,      -- dateCheckOut - smalldatetime
            @IDTable2, -- idTable - int
            0          -- status - int
            );
        SELECT @secondBillID = MAX(id)
        FROM dbo.Bill
        WHERE idTable = @IDTable2
              AND status = 0;
    END;

    SELECT @isSecondTableEmpty = COUNT(*)
    FROM dbo.billInfo
    WHERE idBill = @secondBillID;


    SELECT id
    INTO IDBillInfoTable
    FROM dbo.billInfo
    WHERE idBill = @secondBillID;
    UPDATE dbo.billInfo
    SET idBill = @secondBillID
    WHERE idBill = @firstBillID;
    UPDATE dbo.billInfo
    SET idBill = @firstBillID
    WHERE id IN
          (
              SELECT * FROM dbo.IDBillInfoTable
          );

    DROP TABLE dbo.IDBillInfoTable; -- dùng xong xóa đi để dùng lại

    IF (@isFirstTableEmpty = 0)
        UPDATE dbo.tableSeat
        SET status = N'Trống'
        WHERE id = @IDTable2;
    IF (@isSecondTableEmpty = 0)
        UPDATE dbo.tableSeat
        SET status = N'Trống'
        WHERE id = @IDTable1;
END;
GO
/****** Object:  StoredProcedure [dbo].[UpdateAccount]    Script Date: 8/8/2019 9:07:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[UpdateAccount]
@userName NVARCHAR(100), @displayName NVARCHAR(100), @password NVARCHAR(100), @newpassword NVARCHAR(100)
AS
BEGIN
	DECLARE @isRightPass INT
	SELECT @isRightPass = COUNT(*) FROM dbo.Account WHERE userName = @userName AND password = @password

	IF(@isRightPass >=1)
	BEGIN
		IF(@newpassword IS NULL AND @newpassword = ' ') -- nếu không nhập mật khẩu mới thì chỉ đổi tên hiển thị
			BEGIN
				UPDATE dbo.Account SET displayName = @displayName WHERE userName = @userName
			END
		ELSE
			BEGIN
				UPDATE dbo.Account SET displayName = @displayName , password = @newpassword WHERE userName = @userName
			END
	END
END
GO
/****** Object:  StoredProcedure [dbo].[UserLogin]    Script Date: 8/8/2019 9:07:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UserLogin]
@userName NVARCHAR(100),
@password NVARCHAR(100)
AS
BEGIN
	SELECT * FROM dbo.Account WHERE userName = @userName AND password = @password
END
GO
USE [master]
GO
ALTER DATABASE [CoffeeShop] SET  READ_WRITE 
GO
