﻿using CoffeeShop.Data_Access_Layer;
using CoffeeShop.Entity;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Threading;
using System.Windows.Forms;

namespace CoffeeShop
{
    public partial class TableManager : Form
    {
        private Account loginAccount;
        //dùng cái này để truyền loại tài khoản qua từ form login
        // tương tự chỉ khi là admin thì mới đc quyền sửa thông tin cá nhân nên
        // cái account này truyền tượng tự với form thông tin cá nhân
        public Account LoginAccount
        {
            get
            {
                return loginAccount;
            }

            set
            {
                loginAccount = value;
                ChangeAccount(LoginAccount.Type);
            }
        }

        public TableManager(Account acc)
        {
            InitializeComponent();

            this.LoginAccount = acc;
            //MessageBox.Show("Xin chào " + acc.Displayname, "Thông báo");
            loadTable();
            loadCategory();
            loadComboboxTable(cbSwitchTable);
        }

        #region Methods

        private void ChangeAccount(int type)
        {
            adminToolStripMenuItem.Enabled = type == 1;
            // như một cái khóa để cho phép vào phần quản lí
            // nếu type = 1: admin ==> đc vào else là staff thì không

            thôngTinToolStripMenuItem.Text += " ( " + LoginAccount.Displayname + ")";
        }

        private void loadTable()
        {
            flpTable.Controls.Clear();
            List<Table> tableList = TableAccess.Instance.LoadTableList();
            foreach (Table item in tableList)
            {
                Button btn = new Button() { Width = TableAccess.tableWidth, Height = TableAccess.tableHeight};
                if (item.Status == "Trống")
                {
                    btn.BackColor = Color.MistyRose;
                }
                else btn.BackColor = Color.Salmon;

                btn.Click += Btn_Click;
                btn.Tag = item;

                btn.Text = item.Name + Environment.NewLine /* đây là xuống dòng*/ + item.Status;

                flpTable.Controls.Add(btn);
            } 
        }

        void showBill(int idTable)
        {
            lsvBill.Items.Clear();
            List<Statistic> list = StatisticAccess.Instance.GetListStatisticByTable(idTable);

            float totalPrice = 0;

            foreach (Statistic item in list)
            {
                ListViewItem lsvItem = new ListViewItem(item.FoodName.ToString());
                lsvItem.SubItems.Add(item.Count.ToString());
                lsvItem.SubItems.Add(item.Price.ToString());
                lsvItem.SubItems.Add(item.TotalPrice.ToString());

                totalPrice += item.TotalPrice;

                lsvBill.Items.Add(lsvItem);
            }
            //set vùng về Vn
            CultureInfo culture = new CultureInfo("vi-VN");
            //set thread hiện tại về culture
            Thread.CurrentThread.CurrentCulture = culture;
            txbTotalPrice.Text = totalPrice.ToString("c", culture); //c: currency
        }

        void loadCategory()
        {
            List<Category> listCategory = CategoryAccess.Instance.getListCategory();
            cbCategory.DataSource = listCategory;//lấy cả thông tin từ 1 row và không biết hiển thị gì
            // dùng dòng display member = tên column cần lấy
            cbCategory.DisplayMember = "name";
        }

        void loadFoodListByCategoryID(int id)
        {
            List<Food> listFood = FoodAccess.Instance.GetFoodListByCategoryID(id);
            cbFood.DataSource = listFood;
            cbFood.DisplayMember = "name";
        }

        void loadComboboxTable(ComboBox cb)
        {
            cb.DataSource = TableAccess.Instance.LoadTableList();
            cb.DisplayMember = "name";
        }

        #endregion

        #region Events

        private void Btn_Click(object sender, EventArgs e)
        {
            int idTable = ((sender as Button).Tag as Table).ID;
            lsvBill.Tag = (sender as Button).Tag;
            showBill(idTable);
        }

        private void thôngTinCáNhânToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AccountProfile f = new AccountProfile(LoginAccount);
            f.UpdateAccount += F_UpdateAccount;
            f.ShowDialog();
            //truyền tài khoản hiện có qua form dùng để chỉnh sửa thông tin do chỉ có admin 
            //mới có quyền
        }

        private void F_UpdateAccount(object sender, AccountEven e)
        {
            thôngTinToolStripMenuItem.Text = " ( " + e.Acc.Displayname + ")";
        }

        private void adminToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AdminProfile f = new AdminProfile();
            f.loginAcc = loginAccount;
            //food changes
            f.InsertFood += F_InsertFood;
            f.UpdateFood += F_UpdateFood;
            f.DeleteFood += F_DeleteFood;
            //

            //food category changes
            f.InsertCategory += F_InsertCategory;
            f.UpdateCategory += F_UpdateCategory;
            f.DeleteCategory += F_DeleteCategory;
            //

            //table changes
            f.InsertTable += F_InsertTable;
            f.UpdateTable += F_UpdateTable;
            f.DeleteTable += F_DeleteTable;
            //

            //AccountChanges
            
            //f.show();
            f.ShowDialog();
        }

        private void F_DeleteTable(object sender, EventArgs e)
        {
            loadComboboxTable(cbSwitchTable);
            if (lsvBill.Tag != null)
                showBill((lsvBill.Tag as Table).ID);
            loadTable();
        }

        private void F_UpdateTable(object sender, EventArgs e)
        {
            //loadComboboxTable(cbSwitchTable);
            if (lsvBill.Tag != null)
                showBill((lsvBill.Tag as Table).ID);
            loadTable();
        }

        private void F_InsertTable(object sender, EventArgs e)
        {
            loadComboboxTable(cbSwitchTable);
            if (lsvBill.Tag != null)
                showBill((lsvBill.Tag as Table).ID);
            loadTable();
        }

        private void F_DeleteCategory(object sender, EventArgs e)
        {
            loadCategory();
            if (lsvBill.Tag != null)
                showBill((lsvBill.Tag as Table).ID);
            loadTable();
        }

        private void F_UpdateCategory(object sender, EventArgs e)
        {
            loadCategory();
            if (lsvBill.Tag != null)
                showBill((lsvBill.Tag as Table).ID);
            loadTable();
        }

        private void F_InsertCategory(object sender, EventArgs e)
        {
            loadCategory();
            if (lsvBill.Tag != null)
                showBill((lsvBill.Tag as Table).ID);
            loadTable();
        }
       

        private void F_DeleteFood(object sender, EventArgs e)
        {
            loadFoodListByCategoryID((cbCategory.SelectedItem as Category).Id);
            if(lsvBill.Tag != null)
                showBill((lsvBill.Tag as Table).ID);
            loadTable();
        }

        private void F_UpdateFood(object sender, EventArgs e)
        {
            loadFoodListByCategoryID((cbCategory.SelectedItem as Category).Id);
            if (lsvBill.Tag != null)
                showBill((lsvBill.Tag as Table).ID);
        }
        private void F_InsertFood(object sender, EventArgs e)
        {
            loadFoodListByCategoryID((cbCategory.SelectedItem as Category).Id);
            if (lsvBill.Tag != null)
                showBill((lsvBill.Tag as Table).ID);
        }

        private void đăngXuấtToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cbCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            int Id = 0;

            ComboBox cb = sender as ComboBox;
            if (cb.SelectedItem == null)
                return;
            Category selected = cb.SelectedItem as Category;
            Id = selected.Id;

            loadFoodListByCategoryID(Id);
        }

        private void btnAddFood_Click(object sender, EventArgs e)
        {
            Table table = lsvBill.Tag as Table;

            if(table == null)
            {
                MessageBox.Show("Vui lòng chọn bàn trước khi thêm món!", "Thông báo");
                return;
            }

            int idBill = BillAccess.Instance.GetUncheckedBillIdByTableId(table.ID);
            int idFood = (cbFood.SelectedItem as Food).Id;
            int quantity = (int)nmFood.Value;

            ///có 3 trường hợp ở chỗ thêm món.
            ///TH1: nếu tồn tại bill và món chưa có trong bill thì tạo và thêm
            ///TH2: nếu có bill và món chưa có trong bill thì thêm món
            ///TH3: nếu có bill và có món thì chỉ cần update lại

            if (idBill == -1)//Bill chưa tồn tại
            {
                BillAccess.Instance.InsertBill(table.ID);
                BillInfoAccess.Instance.insertBillInfo(BillAccess.Instance.GetMaxIdBill(), idFood, quantity);
                ///đoạn này sẽ chưa biết được thêm BillInfo cho cái idBill nào vì 
                ///chưa có gì lấy ra giả lại gtri của cái bill trên cái bàn có table.ID
                ///nhưng do thiết kế server idBill identity ==> tự tăng
                ///nên id bill là max
            }
            else
            {
                //đây là trường hợp nếu tồn tại rồi thì chỉ cần lấy idBill ở trên không cần tạo mới
                BillInfoAccess.Instance.insertBillInfo(idBill, idFood, quantity);
            }
            showBill(table.ID);
            loadTable();
        }

        private void btnCheckOut_Click(object sender, EventArgs e)
        {
            Table table = lsvBill.Tag as Table;
            int idBill = BillAccess.Instance.GetUncheckedBillIdByTableId(table.ID);
            int discount = (int)nmDiscount.Value;
            ///double totalPrice = Convert.ToDouble(txbTotalPrice.Text.ToString());
            ///đoạn code trên lỗi do chuyển đơn vị tiền tệ thành có chữ "đ" ở cuối giá tiền
            ///nên convert fail
            ///nên cần phải dùng tách chuỗi theo dấu "," và lấy phần tử đầu tiền - phần tử [0]
            double totalPrice = Convert.ToDouble(txbTotalPrice.Text.Split(',')[0]);
            double finalPrice = totalPrice - (totalPrice / 100) * discount;

            if (lsvBill.Tag as Table == null)
            {
                MessageBox.Show("Vui lòng chọn bàn trước!", "Thông báo");
                return;
            }

            if (idBill != -1)
            {
                if (MessageBox.Show(string.Format("Bạn có chắc muốn thanh toán hóa đơn cho bàn {0}\n Tổng tiền: {1}đ, giảm giá: {2}%\n  Còn lại: {3}đ", table.Name, totalPrice, discount, finalPrice), "Thông báo", MessageBoxButtons.OKCancel) == System.Windows.Forms.DialogResult.OK)
                {
                    BillAccess.Instance.ChekOut(idBill, discount, (float)finalPrice);
                    showBill(table.ID);
                    loadTable();
                }
            }
        }

        private void btnDiscount_Click(object sender, EventArgs e)
        {
            if (nmDiscount.Enabled == false)
                nmDiscount.Enabled = true;
            else
                nmDiscount.Enabled = false;
        }

        private void btnSwitchtable_Click(object sender, EventArgs e)
        {
            if (lsvBill.Tag as Table == null)
            {
                MessageBox.Show("Vui lòng chọn bàn trước!", "Thông báo");
                return;
            }
            int idTable1 = (lsvBill.Tag as Table).ID;
            int idTable2 = (cbSwitchTable.SelectedItem as Table).ID;
            
            if (MessageBox.Show("Bạn có thực sự muốn chuyển hai bàn này? ", "Thông báo", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                TableAccess.Instance.SwitchTable(idTable1, idTable2);
                loadTable();
            }
        }
        #endregion
    }
}
