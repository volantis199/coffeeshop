﻿using CoffeeShop.Data_Access_Layer;
using CoffeeShop.Entity;
using System;
using System.Windows.Forms;

namespace CoffeeShop
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();

        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            string userName = txbUserName.Text.Trim();
            string password = txbPassword.Text.Trim();
            if(login(userName, password))
            {
                Account loginAccount = AccountAccess.Instance.GetAccountByUserName(userName);
                TableManager f = new TableManager(loginAccount);
                this.Hide();
                f.ShowDialog();
                this.Show();
            }
            else
            {
                MessageBox.Show("Đăng nhập thất bại!");
            }

        }

        private bool login(string userName, string password)
        {
            return AccountAccess.Instance.login(userName, password);
        }

        private void Login_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(MessageBox.Show("Bạn có thực sự muốn thoát chương trình?", "Thông báo", MessageBoxButtons.OKCancel) != DialogResult.OK)
            {
                //nếu người dùng ấn vô cancel thì không cho phép xảy ra closing
                e.Cancel = true;
            }
        }
    }
}
