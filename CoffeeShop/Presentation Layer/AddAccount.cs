﻿using CoffeeShop.Data_Access_Layer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CoffeeShop.Presentation_Layer
{
    public partial class AddAccount : Form
    {
        public AddAccount()
        {
            InitializeComponent();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            string userName = txbUserName.Text;
            string displayName = txbDisplayName.Text;
            string pass = txbPass.Text;
            string rePass = txbRePass.Text;
            int type = 0;
            if (cbAccountType.SelectedItem.ToString() == "Quản lý")
                type = 1;

            if(pass != rePass)
            {
                MessageBox.Show("Vui lòng nhập đúng mật khẩu!", "Thông báo");
            }
            else
            {
                if(AccountAccess.Instance.InsertAccount(userName, displayName, pass, type))
                {
                    MessageBox.Show("Thêm tài khoản thành công!", "Thông báo");
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Vui lòng chọn tên hiển thị khác!", "Thông báo");
                }
            }
        }

        //private event EventHandler insertAccount;
        //public event EventHandler InsertAccount
        //{
        //    add { insertAccount += value; }
        //    remove { insertAccount -= value; }
        //}
    }
}
