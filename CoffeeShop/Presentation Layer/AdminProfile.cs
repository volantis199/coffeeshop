﻿using CoffeeShop.Data_Access_Layer;
using CoffeeShop.Entity;
using CoffeeShop.Presentation_Layer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace CoffeeShop
{
    public partial class AdminProfile : Form
    {
        //không cho phép xóa tk hiện tại
        public Account loginAcc;

        BindingSource foodList = new BindingSource();
        BindingSource categoryList = new BindingSource();
        BindingSource tableList = new BindingSource();
        BindingSource accountList = new BindingSource();

        public AdminProfile()
        {
            InitializeComponent();
            Load();
        }
        #region Methods

        private void Load()
        {
            dtgvFood.DataSource = foodList;
            dtgvFoodCategory.DataSource = categoryList;
            dtgvTable.DataSource = tableList;
            dtgvAccount.DataSource = accountList;

            LoadListFood();
            LoadListCategory();
            LoadListTable();
            LoadListAccount();

            LoadFoodCategoryIntoCombobox(cbFoodCategory);
            LoadDateTimePicker();
            LoadListBillByDate(dtpkFromDate.Value, dtpkToDate.Value);
            //LoadTableStatusIntoCombobox(cbTableStatus);
            LoadAccountTypeIntoCombobox(cbAccountType);


            AddFoodBinding();
            AddFoodCategoryBinding();
            AddTableBinding();
            AddAccountBinding();
        }

        List<Food> SearchFoodByName(string name)
        {
            List<Food> list = FoodAccess.Instance.GetFoodListByName(name);

            return list;
        }
        #endregion

        #region Events Bindings
        void AddFoodBinding()
        {
            //tên đối tượng. DataBindings.Add(new Binding("tên thuộc tính cần add", nguồn thêm, "giá trị ở nguồn để thêm"))
            txbFoodName.DataBindings.Add(new Binding("Text", dtgvFood.DataSource, "name", true, DataSourceUpdateMode.Never));
            txbFoodID.DataBindings.Add(new Binding("Text", dtgvFood.DataSource, "id", true, DataSourceUpdateMode.Never));
            nmFoodPrice.DataBindings.Add(new Binding("Value", dtgvFood.DataSource, "price", true, DataSourceUpdateMode.Never));

        }

        void AddFoodCategoryBinding()
        {
            txbFoodCategoryID.DataBindings.Add(new Binding("Text", dtgvFoodCategory.DataSource, "id", true, DataSourceUpdateMode.Never));
            txbFoodCategoryName.DataBindings.Add(new Binding("Text", dtgvFoodCategory.DataSource, "name", true, DataSourceUpdateMode.Never));
        }

        void AddTableBinding()
        {
            txbTableID.DataBindings.Add(new Binding("Text", dtgvTable.DataSource, "id", true, DataSourceUpdateMode.Never));
            txbTableName.DataBindings.Add(new Binding("Text", dtgvTable.DataSource, "name", true, DataSourceUpdateMode.Never));
            cbTableStatus.DataBindings.Add(new Binding("Text", dtgvTable.DataSource, "status", true, DataSourceUpdateMode.Never));
        }
        
        void AddAccountBinding()
        {
            txbUserName.DataBindings.Add(new Binding("Text", dtgvAccount.DataSource, "userName", true, DataSourceUpdateMode.Never));
            txbDisplayName.DataBindings.Add(new Binding("Text", dtgvAccount.DataSource, "displayName", true, DataSourceUpdateMode.Never));
            cbAccountType.DataBindings.Add(new Binding("Text", dtgvAccount.DataSource, "type", true, DataSourceUpdateMode.Never));
        }
        #endregion

        #region Load materials

        private void LoadDateTimePicker()
        {
            DateTime today = DateTime.Now;
            dtpkFromDate.Value = new DateTime(today.Year, today.Month, 1);
            /// đây là cách aovailon nhất từng thấy để lấy auto là ngày cuối tháng
            /// bằng cách cộng ngày đầu tháng thêm 1 tháng và trừ đi 1 ngày ==> cuối tháng
            /// @@ XD
            dtpkToDate.Value = dtpkFromDate.Value.AddMonths(1).AddDays(-1);
        }

        private void LoadListBillByDate(DateTime date1, DateTime date2)
        {
            dtgvBill.DataSource = BillAccess.Instance.GetListBillByDate(date1, date2);
        }

        private void LoadListFood()
        {
            foodList.DataSource = FoodAccess.Instance.GetFoodList();
        }

        private void LoadListCategory()
        {
            categoryList.DataSource = CategoryAccess.Instance.getListCategory();
        }

        private void LoadFoodCategoryIntoCombobox(ComboBox cb)
        {
            cb.DataSource = CategoryAccess.Instance.getListCategory();
            cb.DisplayMember = "Name";
        }

        private void LoadListTable()
        {
            tableList.DataSource = TableAccess.Instance.GetListTable();
        }

        private void LoadTableStatusIntoCombobox(ComboBox cb) ///????
        {
            var dataSource = TableAccess.Instance.GetListTable().Select(x => x.Status).Distinct();
            if (dataSource != null)
            {
                cb.DataSource = dataSource.ToList();
                cb.DisplayMember = "status";
            }
        }

        private void LoadListAccount()
        {
            accountList.DataSource = AccountAccess.Instance.getListAccount();
        }
        /// <summary>
        /// linq == check for null value
        /// </summary>
        /// <param name="cb"></param>
        private void LoadAccountTypeIntoCombobox(ComboBox cb)
        {
            var dataSource = AccountAccess.Instance.getListAccount().Select(x => x.Type).Distinct();
            if (dataSource != null)
            {
                cb.DataSource = dataSource.ToList();
                cb.DisplayMember = "accountType";
            }
            //MessageBox.Show("AAA" + cbAccountType.DisplayMember.ToString());
        }

        #endregion

        #region Events Food
        private void btnViewFood_Click(object sender, EventArgs e)
        {
            LoadListFood();
        }

        private void txbFoodID_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (dtgvFood.SelectedCells.Count > 0)
                {
                    int id = (int)dtgvFood.SelectedCells[0].OwningRow.Cells["idCategory"].Value;
                    Category category = CategoryAccess.Instance.getCategoryByID(id);
                    cbFoodCategory.SelectedItem = category;

                    int index = 0;
                    int i = 0;
                    foreach (Category item in cbFoodCategory.Items)
                    {
                        if (item.Id == category.Id)
                        {
                            index = i;
                            break;
                        }
                        i++;
                    }

                    cbFoodCategory.SelectedIndex = index;
                }
            }
            catch{}
        }

        private void btnAddFood_Click(object sender, EventArgs e)
        {

            string name = txbFoodName.Text;
            int idCategory = (cbFoodCategory.SelectedItem as Category).Id;
            float price = (float)nmFoodPrice.Value;

            if (FoodAccess.Instance.InsertFood(name, idCategory, price))
            {
                MessageBox.Show("Thêm món thành công!", "Thông báo");
                LoadListFood();
                insertFood?.Invoke(this, new EventArgs());
                //if (insertFood != null)
                //{
                //    insertFood(this, new EventArgs());
                //}
            }
            else
            {
                MessageBox.Show("Vui lòng kiểm tra lại!", "Thông báo");
            }
        }

        private void btnEditFood_Click(object sender, EventArgs e)
        {
            int idFood = Convert.ToInt32(txbFoodID.Text);
            string name = txbFoodName.Text;
            int idCategory = (cbFoodCategory.SelectedItem as Category).Id;
            float price = (float)nmFoodPrice.Value;

            if (FoodAccess.Instance.UpdateFood(idFood, name, idCategory, price))
            {
                MessageBox.Show("Cập nhật món thành công!", "Thông báo");
                LoadListFood();
                if (updateFood != null)
                {
                    updateFood(this, new EventArgs());
                }
            }
            else
            {
                MessageBox.Show("Vui lòng kiểm tra lại!", "Thông báo");
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            foodList.DataSource = SearchFoodByName(txbSearchFood.Text);
        }

        private void btnDeleteFood_Click(object sender, EventArgs e)
        {
            int idFood = Convert.ToInt32(txbFoodID.Text);

            if (FoodAccess.Instance.DeleteFood(idFood))
            {
                MessageBox.Show("Xóa món thành công!", "Thông báo");
                LoadListFood();
                if (deleteFood != null)
                {
                    deleteFood(this, new EventArgs());
                }
            }
            else
            {
                MessageBox.Show("Vui lòng kiểm tra lại!", "Thông báo");
            }
        }


        private event EventHandler insertFood;
        public event EventHandler InsertFood
        {
            add { insertFood += value; }
            remove { insertFood -= value; }
        }

        private event EventHandler updateFood;
        public event EventHandler UpdateFood
        {
            add { updateFood += value; }
            remove { updateFood -= value; }
        }


        private event EventHandler deleteFood;
        public event EventHandler DeleteFood
        {
            add { deleteFood += value; }
            remove { deleteFood -= value; }
        }


        #endregion

        #region Events Category
        private void btnViewCategory_Click(object sender, EventArgs e)
        {
            LoadListCategory();
        }

        private void btnAddCategory_Click(object sender, EventArgs e)
        {
            string name = txbFoodCategoryName.Text;
            if (CategoryAccess.Instance.insertCategory(name))
            {
                MessageBox.Show("Thêm danh mục thành công!", "Thông báo");
                LoadListCategory();
                LoadFoodCategoryIntoCombobox(cbFoodCategory);

                if (insertCategory != null)
                    insertCategory(this, new EventArgs());
            }
            else
            {
                MessageBox.Show("Vui lòng kiểm tra lại!", "Thông báo");
            }
        }

        private void btnEditCategory_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(txbFoodCategoryID.Text);
            string name = txbFoodCategoryName.Text;
            if (CategoryAccess.Instance.updateCategory(id, name))
            {
                MessageBox.Show("Cập nhật danh mục thành công!", "Thông báo");
                LoadListCategory();
                LoadFoodCategoryIntoCombobox(cbFoodCategory);

                if (updateCategory != null)
                    updateCategory(this, new EventArgs());
            }
            else
            {
                MessageBox.Show("Vui lòng kiểm tra lại!", "Thông báo");
            }
        }

        private void btnDeleteCategory_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(txbFoodCategoryID.Text);

            if (CategoryAccess.Instance.deleteCategory(id))
            {
                MessageBox.Show("Xóa danh mục thành công!", "Thông báo");
                LoadListCategory();
                LoadFoodCategoryIntoCombobox(cbFoodCategory);

                if (deleteCategory != null)
                {
                    deleteCategory(this, new EventArgs());
                }
            }
            else
            {
                MessageBox.Show("Vui lòng kiểm tra lại!", "Thông báo");
            }
        }

        private event EventHandler insertCategory;
        public event EventHandler InsertCategory
        {
            add { insertCategory += value; }
            remove { insertCategory -= value; }
        }

        private event EventHandler updateCategory;
        public event EventHandler UpdateCategory
        {
            add { updateCategory += value; }
            remove { updateCategory -= value; }
        }


        private event EventHandler deleteCategory;
        public event EventHandler DeleteCategory
        {
            add { deleteCategory += value; }
            remove { deleteCategory -= value; }
        }

        #endregion

        #region Events Table

        private void btnViewtable_Click(object sender, EventArgs e)
        {
            LoadListTable();
        }

        private void btnAddTable_Click(object sender, EventArgs e)
        {
            string name = txbTableName.Text;
            string status = cbTableStatus.SelectedItem.ToString();
            if (TableAccess.Instance.InsertTable(name, status))
            {
                MessageBox.Show("Thêm bàn mới thành công!", "Thông báo");
                LoadListTable();
                if (insertTable != null)
                    insertTable(this, new EventArgs());
            }
            else
            {
                MessageBox.Show("Vui lòng kiểm tra lại!", "Thông báo");
            }
        }

        private void btnDeleteTable_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(txbTableID.Text);
            if (TableAccess.Instance.DeleteTable(id))
            {
                MessageBox.Show("Xóa bàn thành công!", "Thông báo");
                LoadListTable();
                if (deleteTable != null)
                    deleteTable(this, new EventArgs());
            }
            else
            {
                MessageBox.Show("Vui lòng kiểm tra lại!", "Thông báo");
            }
        }
        private void btnEditTable_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(txbTableID.Text);
            string name = txbTableName.Text;
            string status = cbTableStatus.SelectedItem.ToString();

            if (TableAccess.Instance.UpdateTable(id, name, status))
            {
                MessageBox.Show("Cập nhật bàn thành công!", "Thông báo");
                LoadListTable();
                //LoadTableStatus(cbTableStatus);

                if (updateTable != null)
                    updateTable(this, new EventArgs());
            }
            else
            {
                MessageBox.Show("Vui lòng kiểm tra lại!", "Thông báo");
            }
        }

        private event EventHandler insertTable;
        public event EventHandler InsertTable
        {
            add { insertTable += value; }
            remove { insertTable -= value; }
        }

        private event EventHandler updateTable;
        public event EventHandler UpdateTable
        {
            add { updateTable += value; }
            remove { updateTable -= value; }
        }


        private event EventHandler deleteTable;
        public event EventHandler DeleteTable
        {
            add { deleteTable += value; }
            remove { deleteTable -= value; }
        }

        #endregion

        #region Events Account
        private void txbUserName_TextChanged(object sender, EventArgs e)
        {
            if (dtgvAccount.SelectedCells.Count > 0)
            {
                string userName = dtgvAccount.SelectedCells[2].OwningRow.Cells["userName"].Value.ToString();

                Account acc = AccountAccess.Instance.getAccountTypeByUserName(userName);
                cbAccountType.SelectedItem = acc;

                int index = 0;
                int i = 0;
                foreach (Account item in cbAccountType.Items)
                {
                    if (item.Type == acc.Type)
                    {
                        index = i;
                        break;
                    }
                    i++;
                }

                cbAccountType.SelectedIndex = index;
            }
        }

        private void btnAddAccount_Click(object sender, EventArgs e)
        {
            AddAccount f = new AddAccount();
            this.Hide();
            f.ShowDialog();
        }

        private void btnViewAccount_Click(object sender, EventArgs e)
        {
            LoadListAccount();
        }

        private void btnResetPassword_Click(object sender, EventArgs e)
        {
            string userName = txbUserName.Text;
            if (AccountAccess.Instance.ResetPassword(userName))
            {
                MessageBox.Show("Reset mật khẩu thành công!", "Thông báo");
                LoadListAccount();
            }
            else
            {
                MessageBox.Show("Có lỗi vui lòng thử lại!", "Thông báo");
            }
        }
        private void btnDeleteAccount_Click(object sender, EventArgs e)
        {
            string userName = txbUserName.Text;
            if(loginAcc.UserName.Equals(userName))
            {
                MessageBox.Show("Không thể xóa chính tài khoản này!", "Thông báo");
                return;
            }
            if (AccountAccess.Instance.DeleteAccount(userName))
            {
                MessageBox.Show("Xóa tài khoản thành công!", "Thông báo");
                LoadListAccount();
            }
            else
            {
                MessageBox.Show("Có lỗi vui lòng thử lại!", "Thông báo");

            }
        }
        private void btnEditAccount_Click(object sender, EventArgs e)
        {
            string userName = txbUserName.Text;
            string displayName = txbDisplayName.Text;
            int type = Convert.ToInt32(cbAccountType.SelectedItem.ToString());
            if (AccountAccess.Instance.EditAccount(userName, displayName, type))
            {
                MessageBox.Show("Sửa tài khoản thành công!", "Thông báo");
                LoadListAccount();
            }
            else
            {
                MessageBox.Show("Có lỗi vui lòng thử lại!", "Thông báo");
            }
        }


        #endregion

        #region Load Bill
        private void btnViewBill_Click(object sender, EventArgs e)
        {
            LoadListBillByDate(dtpkFromDate.Value, dtpkToDate.Value);
        }

        private void BtnFirstBillPage_Click(object sender, EventArgs e)
        {
            txbCurrentBillPage.Text = "1";

        }

        private void BtnLastBillPage_Click(object sender, EventArgs e)
        {
            int totalPage = BillAccess.Instance.GetNumBillByDate(dtpkFromDate.Value, dtpkToDate.Value);
            int billPerPage = 10;

            int lastPage = totalPage / billPerPage;
            if(totalPage % 10 != 0)
            {
                lastPage++;
            }

            txbCurrentBillPage.Text = lastPage.ToString();
        }

        private void TxbCurrentBillPage_TextChanged(object sender, EventArgs e)
        {
            dtgvBill.DataSource = BillAccess.Instance.GetListBillByDateAndPage(dtpkFromDate.Value, dtpkToDate.Value, Convert.ToInt32(txbCurrentBillPage.Text));
        }

        private void BtnPreviousBillPage_Click(object sender, EventArgs e)
        {
            int currentPage = Convert.ToInt32(txbCurrentBillPage.Text);
            if(currentPage > 1)
            {
                txbCurrentBillPage.Text = (currentPage - 1).ToString();
            }
        }

        private void BtnNextBillPage_Click(object sender, EventArgs e)
        {
            int currentPage = Convert.ToInt32(txbCurrentBillPage.Text);
            int totalPage = BillAccess.Instance.GetNumBillByDate(dtpkFromDate.Value, dtpkToDate.Value);
            if (currentPage < totalPage)
                txbCurrentBillPage.Text = (currentPage++).ToString();
        }

        private void DtpkFromDate_ValueChanged(object sender, EventArgs e)
        {
            dtgvBill.DataSource = BillAccess.Instance.GetListBillByDateAndPage(dtpkFromDate.Value, dtpkToDate.Value, Convert.ToInt32(txbCurrentBillPage.Text));
        }

        private void DtpkToDate_ValueChanged(object sender, EventArgs e)
        {
            dtgvBill.DataSource = BillAccess.Instance.GetListBillByDateAndPage(dtpkFromDate.Value, dtpkToDate.Value, Convert.ToInt32(txbCurrentBillPage.Text));
        }

        #endregion
    }
}
