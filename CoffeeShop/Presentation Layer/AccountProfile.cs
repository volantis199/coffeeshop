﻿using CoffeeShop.Data_Access_Layer;
using CoffeeShop.Entity;
using System;
using System.Windows.Forms;

namespace CoffeeShop
{
    public partial class AccountProfile : Form
    {
        private Account acc;
        public AccountProfile(Account acc)
        {
            InitializeComponent();
            this.Acc = acc;
        }

        public Account Acc
        {
            get
            {
                return acc;
            }

            set
            {
                acc = value;
                ChangeAccount(acc);
            }
        }

        private void ChangeAccount(Account acc)
        {
            txbUserName.Text = acc.UserName;
            txbDisplayName.Text = acc.Displayname;
            // không ĐƯỢC HIỆN MẬT KHẨU !!
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void EditAccount()
        {
            ///có vẫn đề ở đay như sau
            ///TH1: Nếu chỉ thay đổi displayName thì chỉ cần nhập mật khẩu
            ///TH2: CÒn nếu muốn thay đổi mật khẩu thì phải nhập tất cả
            ///sẽ có cách là viết stored procedure để truyền tất cả nhưng  KHÔNG CẦN!!
            ///nếu nhập mật khẩu mới, chỉ cần check giữa ô mật khẩu cũ và mật khẩu mới là được
            string userName = txbUserName.Text;
            string displayName = txbDisplayName.Text.Trim();
            string password = txbPassword.Text;
            string newPassword = txbNewPassword.Text;
            string reEnterPass = txbReEnter.Text;


            if (!newPassword.Equals(reEnterPass))
            {
                //nếu hai ô mật khẩu này không giống nhau
                MessageBox.Show("Mật khẩu mới không khớp, vui lòng nhập lại", "Thông báo");
            }
            else
            {
                //Có 2 case:
                //case1: hai ô này đều trống case2: không giống nhau
                if (AccountAccess.Instance.UpdateAccount(userName, displayName, password, newPassword))
                {
                    MessageBox.Show("Cập nhật thành công!", "Thông báo");
                    if(updateAccount != null)
                    {
                        updateAccount(this, new AccountEven(AccountAccess.Instance.GetAccountByUserName(userName)));
                    }
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Có lỗi \n Cập nhật thất bại", "Thông báo");
                }
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            EditAccount();
        }

        private event EventHandler <AccountEven> updateAccount;
        public event EventHandler<AccountEven> UpdateAccount
        {
            add
            {
                updateAccount += value;
            }
            remove
            {
                updateAccount -= value;
            }
        }
    }

    public class AccountEven: EventArgs
    {
        private Account acc;

        public AccountEven(Account a)
        {
            this.Acc = a;
        }
        public Account Acc
        {
            get
            {
                return acc;
            }

            set
            {
                acc = value;
            }
        }
    }
}
